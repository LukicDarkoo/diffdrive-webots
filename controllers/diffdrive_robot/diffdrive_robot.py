# To change radius and width of the wheels go to:
# Robot > children > HingeJoint > endPoint Solid > boundingObject > children > Shape > geometry
#
# To change translation or rotation of the wheel go to:
# Robot > children > HingeJoint > endPoint Solid > boundingObject

from controller import Robot


TIMESTEP = 64
LEFT_STEPS = 10
RIGHT_STEPS = 10


def main():
    robot = Robot()

    # Init differential drive motors
    left_motor = robot.getMotor('left wheel motor')
    right_motor = robot.getMotor('right wheel motor')
    left_motor.setPosition(LEFT_STEPS)
    right_motor.setPosition(RIGHT_STEPS)


    while (robot.step(TIMESTEP) != -1):
        pass

if __name__ == '__main__':
    main()
